<?php
/**
 * Shipping Calculator
 *
 * Plugin Name: Shipping Calculator
 * Description: The plugin adds shipping cost calculator method and pickup method for cities and regions.
 * Author: Artem Terehin <arterehin@gmail.com>
 * Text Domain: shipping-calculator
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

define( 'SC_PLUGIN_FILE', __FILE__ );

/**
 * Check if WooCommerce is active
 */
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
    function sc_methods_add($methods)
    {
        $methods['sc_calculator'] = 'SC_Shipping_Calculator';
        $methods['sc_pickup'] = 'SC_Shipping_Pickup';

        return $methods;
    }

    add_filter('woocommerce_shipping_methods', 'sc_methods_add');

    function sc_methods_init()
    {
        require_once plugin_dir_path(__FILE__) . 'includes/class-sc-point-location.php';
        require_once plugin_dir_path(__FILE__) . 'includes/abstract/abstract-sc-shipping-method.php';
        require_once plugin_dir_path(__FILE__) . 'includes/class-sc-shipping-calculator.php';
        require_once plugin_dir_path(__FILE__) . 'includes/class-sc-shipping-pickup.php';
    }

    add_action('woocommerce_shipping_init', 'sc_methods_init');
}