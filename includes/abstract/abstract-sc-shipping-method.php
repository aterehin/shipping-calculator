<?php
/**
 * Calculator Shipping Method.
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}


abstract class SC_Shipping_Method extends WC_Shipping_Method
{
    static protected $geocode_url = 'https://geocode-maps.yandex.ru/1.x';
    static protected $routes_url = 'http://routes.puffin.shop/route/v1/driving';

    public function __construct($instance_id = 0)
    {
        $this->supports = array(
            'shipping-zones',
            'instance-settings',
        );
        $this->subjects = array(
            array(
                'names' => array('Москва', 'Московская область'),
                'points' => $this->get_coordinates('mkad'),
                'polygon' => $this->get_coordinates('mkad_polygon'),
            ),
            array(
                'names' => array('Санкт-Петербург', 'Ленинградская область'),
                'points' => $this->get_coordinates('kad'),
                'polygon' => $this->get_coordinates('kad_polygon'),
            ),
        );
        $this->point_location = new Point_Location();

        $this->init();
    }

    public function is_available($package)
    {
        if ($package['destination']['city']) {
            $this->geo_object = $this->get_geo_object($package['destination']);
            $this->subject = $this->get_subject($this->geo_object['name']);

            return !empty($this->geo_object) && !empty($this->subject);
        }

        return false;
    }

    protected function get_coordinates($name)
    {
        $json = plugin_dir_path(SC_PLUGIN_FILE) . 'coordinates/' . $name . '.json';

        return is_readable($json) ? json_decode(file_get_contents($json)) : array();
    }

    protected function get_geo_object($destination)
    {
        $object = array();
        $response = wp_remote_get(self::$geocode_url . '?' . http_build_query(array(
                'format' => 'json',
                'lang' => 'ru_RU',
                'apikey' => $this->apikey,
                'geocode' => implode(', ', array(
                    'Россия',
                    $destination['state'],
                    $destination['city'],
                    $destination['address'],
                )),
                'kind' => 'locality',
            )));

        if (is_array($response)) {
            $json = json_decode($response['body'], true);
            $geo_object = $json['response']['GeoObjectCollection']['featureMember'][0]['GeoObject'];

            $object['name'] = $geo_object['metaDataProperty']['GeocoderMetaData']['AddressDetails']['Country']['AdministrativeArea']['AdministrativeAreaName'];
            $object['point'] = explode(' ', $geo_object['Point']['pos']);
        }

        return $object;
    }

    protected function get_subject($city)
    {
        $subjects = array_filter($this->subjects, function ($item, $index) use ($city) {
            return in_array($city, $item['names']);
        }, ARRAY_FILTER_USE_BOTH);

        return !empty($subjects) ? $subjects[key($subjects)] : array();
    }

    protected function get_closest_point($to, $points)
    {
        $distances = array_map(function ($point) use ($to) {
            list($lon1, $lat1) = $point;
            list($lon2, $lat2) = $to;

            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            return $miles;
        }, $points);

        asort($distances);

        return $points[key($distances)];
    }

    protected function get_route($start, $end)
    {
        $response = wp_remote_get(self::$routes_url . '/' . implode(',', $start) . ';' . implode(',', $end));

        return is_array($response) ? json_decode($response['body'], true) : array();
    }
}