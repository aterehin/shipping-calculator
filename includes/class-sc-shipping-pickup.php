<?php
/**
 * Pickup Shipping Method.
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!class_exists('SC_Shipping_Pickup')) {
    class SC_Shipping_Pickup extends SC_Shipping_Method
    {
        public function __construct($instance_id = 0)
        {
            $this->id = 'sc_pickup';
            $this->instance_id = absint($instance_id);
            $this->method_title = __('Pickup');
            $this->method_description = __('Pickup method for cities and regions');

            parent::__construct( $instance_id );
        }

        function init()
        {
            $this->init_form_fields();
            $this->init_settings();

            $this->title = $this->get_option('title');
            $this->apikey = $this->get_option('apikey');
            $this->fixed_cost = $this->get_option('fixed_cost');

            add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
        }

        public function init_form_fields()
        {
            $this->instance_form_fields = array(
                'title' => array(
                    'title' => 'Название метода доставки',
                    'type' => 'text',
                    'default' => $this->method_title,
                ),
                'apikey' => array(
                    'title' => 'Ключ Яндекс API',
                    'type' => 'text',
                    'default' => '',
                ),
                'fixed_cost' => array(
                    'title' => 'Фиксированная стоимость',
                    'type' => 'price',
                    'default' => '0',
                ),
            );
        }

        public function calculate_shipping($package = array())
        {
            $this->add_rate(array(
                'label' => $this->title,
                'cost' => $this->fixed_cost,
                'taxes' => false,
                'package' => $package,
            ));
        }
    }
}