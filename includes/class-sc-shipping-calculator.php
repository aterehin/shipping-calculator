<?php
/**
 * Calculator Shipping Method.
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (!class_exists('SC_Shipping_Calculator')) {
    class SC_Shipping_Calculator extends SC_Shipping_Method
    {
        public function __construct($instance_id = 0)
        {
            $this->id = 'sc_calculator';
            $this->instance_id = absint($instance_id);
            $this->method_title = __('Calculator');
            $this->method_description = __('Shipping cost calculator method based on distance');

            parent::__construct( $instance_id );
        }

        function init()
        {
            $this->init_form_fields();
            $this->init_settings();

            $this->title = $this->get_option('title');
            $this->apikey = $this->get_option('apikey');
            $this->fixed_cost = $this->get_option('fixed_cost');
            $this->distance_cost = $this->get_option('distance_cost');

            add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));
        }

        public function init_form_fields()
        {
            $this->instance_form_fields = array(
                'title' => array(
                    'title' => 'Название метода доставки',
                    'type' => 'text',
                    'default' => $this->method_title,
                ),
                'apikey' => array(
                    'title' => 'Ключ Яндекс API',
                    'type' => 'text',
                    'default' => '',
                ),
                'fixed_cost' => array(
                    'title' => 'Фиксированная стоимость',
                    'type' => 'price',
                    'default' => '0',
                ),
                'distance_cost' => array(
                    'title' => 'Стоимость за КМ',
                    'type' => 'price',
                    'default' => '0',
                ),
            );
        }

        public function calculate_shipping($package = array())
        {
            $rate = array(
                'label' => $this->title,
                'cost' => $this->fixed_cost,
                'taxes' => false,
                'package' => $package,
            );

            $point = $this->geo_object['point'];
            $points = $this->subject['points'];
            $polygon = $this->subject['polygon'];
            $point_location = $this->point_location->point_in_polygon($point, $polygon);

            if ($point_location === 'outside') {
                $route = $this->get_route($this->get_closest_point($point, $points), $point);

                if ($route['code'] === 'Ok' && $route['routes'][0]) {
                    $distance = ceil($route['routes'][0]['distance'] / 1000);
                    $rate['cost'] = $this->fixed_cost + ($this->distance_cost * $distance);
                }
            }

            $this->add_rate($rate);
        }
    }
}